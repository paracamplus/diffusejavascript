# Time-stamp: "2021-03-19 10:16:45 queinnec"

work : nothing 
clean :: cleanMakefile

deploy : .gitlab-ci.yml
	git commit -m "deployed on $$(date -u --iso-8601=seconds)" .
	git push

# 2021 mar 19:
# MOOC is on https://diffusejavascript.edunext.io/
# Teaser page is on https://diffusejavascript.codegradx.org/
# Repository is https://gitlab.com/paracamplus/diffusejavascript
# Doc https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/
# Pages https://paracamplus.gitlab.io/diffusejavascript
# DNS diffusejavascript.codegradx.org   A 35.185.44.232

# #################################### Obsolete
REMOTE  =   diffusejavascript.codegradx.org
install :
	rsync -avuL . ${REMOTE}:/var/www/${REMOTE}/

# end of Makefile
